package com.atguigu.cloud.config;

import feign.Logger;
import feign.Retryer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.util.retry.Retry;

@Configuration
public class FeignConfig {

    @Bean
    public Retryer myRetryer(){
        //不重试
        return Retryer.Default.NEVER_RETRY;
        //最大请求次数为3，初始间隔为100ms，最大间隔为1s
//        return new Retryer.Default(100, 1, 3);
    }

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }
}
