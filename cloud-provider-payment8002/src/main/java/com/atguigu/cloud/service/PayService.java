package com.atguigu.cloud.service;

import com.atguigu.cloud.entities.Pay;
import org.springframework.stereotype.Service;

import java.util.List;

public interface PayService {
    public Integer add(Pay pay);
    public Pay getById(Integer id);
    public Integer delete(Integer id);
    public Integer update(Pay pay);
    public List<Pay> getAll();
}
